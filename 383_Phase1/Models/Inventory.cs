﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _383_Phase1.Models
{
    public class Inventory
    {

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }


        // foreign key
        public int UserID { get; set; }

        public virtual User user { get; set; }
    }
}